﻿using EntidadesNegocio;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccesoNegocio
{
    public class DA_Usuario : ClaseBase
    {
        public List<Usuario> Listar_Preguntas(String Usuario, String Clave)
        {
            List<Usuario> lstLista = new List<Usuario>();
            SqlConnection cn = new SqlConnection(this.CadenaConexion());
            SqlCommand cmd = new SqlCommand("Usp_ValidaUsuario", cn);
            cmd.Parameters.AddWithValue("@Usuario", Usuario);
            cmd.Parameters.AddWithValue("@Clave", Clave);
            cmd.CommandType = CommandType.StoredProcedure;


            SqlDataReader dr = null;

            try
            {
                cn.Open();

                dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        Usuario objReg = new Usuario();
                        //MARCA_BE objMarca = new MARCA_BE();
                        //byte[] bytBLOBData = new byte[dr.GetBytes(1, 0, null, 0, int.MaxValue)];
                        objReg.sUsuario = dr.GetString(0);
                        objReg.sClave = dr.GetString(1);
                        objReg.iToken = dr.GetInt32(2);
                        objReg.sMensaje = dr.GetString(3);
                        //objReg.Des_Pregunta = dr.GetString(2);


                        lstLista.Add(objReg);

                    }



                }
            }
            catch (Exception ex)
            {
                lstLista = new List<Usuario>();
            }
            finally
            {
                dr.Close();

                cn.Close();
            }

            return lstLista;
        }
    
}
}

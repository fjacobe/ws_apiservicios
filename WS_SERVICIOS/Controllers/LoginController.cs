﻿using AccesoNegocio;
using EntidadesNegocio;
using LogicaNegocio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Security.Cryptography;
using static EntidadesNegocio.Usuario;

namespace WS_SERVICIOS.Controllers
{
    public class LoginController : ApiController
    {
        private Usuario BE_Usuario = new Usuario();
        private DA_Usuario DA_Usuario = new DA_Usuario();
        private BL_Usuario BL_Usuario = new BL_Usuario();

        public static Byte[] ConvertStringToByteArray(String s)
        {
            return (new System.Text.UnicodeEncoding()).GetBytes(s);
        }

        [HttpGet]
        public List<Usuario> ObtenerSession(String usu, String Clave)
        {


            //var res = new ObjUsuario() { Usuario = BL_Usuario.Listar() };
            //return res;
            String ClaveEncriptada;
            Byte[] valueToHash = ConvertStringToByteArray(Clave);
            byte[] hashvalue = (new MD5CryptoServiceProvider()).ComputeHash(valueToHash);
            ClaveEncriptada = BitConverter.ToString(hashvalue);
            var res = BL_Usuario.Listar(usu, ClaveEncriptada);
            return res;
        }



        // GET: api/Login
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Login/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Login
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Login/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Login/5
        public void Delete(int id)
        {
        }
    }
}

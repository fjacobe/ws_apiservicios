﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntidadesNegocio
{
    public class Usuario
    {
        private String _sUsuario;
        private String _sClave;
        private Int32 _iToken;
        private String _sMensaje;

        public Int32 iToken
        {
            get { return _iToken; }
            set { _iToken = value; }
        }

        public String sMensaje
        {
            get { return _sMensaje; }
            set { _sMensaje = value; }
        }

        public String sUsuario
        {
            get { return _sUsuario; }
            set { _sUsuario = value; }
        }

        public String sClave
        {
            get { return _sClave; }
            set { _sClave = value; }
        }

        public class ObjUsuario
        {
            public List<Usuario> Usuario;
        }
    }
}
